# Part Finder

A simple tryout project to browse saved parts with Nodejs Express.

## Installation

To clone the project:
```sh
$ git clone https://<username>@bitbucket.org/kodzi/part_finder.git
```

Install the requirements:
```sh
$ npm install
```

Setup MongoDB and create a configs_local.js similar to configs.js

## Setup
Create a data folder inside the project to hold the MongoDB database content
```sh
$ mkdir data
```

Run an MongoDB instance with this folder as the location
```sh
$ mongod --dbpath data/
```

## Usage
Run the project in DEBUG-mode:
OSX & Linux
```sh
$ DEBUG=part_finder:* npm start
```
Windows
```sh
> set DEBUG=part_finder:* & npm start
```

Run the project normally:
```sh
$ npm start
```

Run with auto-reloading using nodemon:
```sh
$ npm install -g nodemon
$ nodemon ./bin/www
```

## Contributing

1. Clone it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a merge request :D

## History

Once upon a time there was a deadline ending today.

## Credits

Caffeine

## License

I don't really care
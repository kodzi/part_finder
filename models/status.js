var mongoose = require('mongoose')
var Schema = mongoose.Schema

var beautifyUnique = require('mongoose-beautiful-unique-validation')

var statusSchema = new Schema({
  code: { type: String, required: true, unique: true },
  description: { type: String, default: '' },
  created_at: Date,
  updated_at: Date
})

// Beautify duplication errors
statusSchema.plugin(beautifyUnique)

statusSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    delete ret._id
    delete ret.__v
    return ret
  }
})

statusSchema.pre('save', function (next) {
  var currentDate = new Date()
  this.updated_at = currentDate
  if (!this.created_at) {
    this.created_at = currentDate
  }
  next()
})
var Status = mongoose.model('Status', statusSchema)

module.exports = Status

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var beautifyUnique = require('mongoose-beautiful-unique-validation')

var partSchema = new Schema({
  part_no: { type: String, required: true, unique: true },
  description: { type: String, default: '' },
  rfid: { type: String, required: true, unique: true },
  status: { type: String, ref: 'Statuscode', default: '' },
  created: Date,
  updated: Date
})

// Beautify duplication errors
partSchema.plugin(beautifyUnique)

partSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    delete ret._id
    delete ret.__v
    return ret
  }
})

partSchema.pre('save', function (next) {
  var currentDate = new Date()
  this.updated = currentDate
  if (!this.created) {
    this.created = currentDate
  }
  next()
})
var Part = mongoose.model('Part', partSchema)

module.exports = Part

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var beautifyUnique = require('mongoose-beautiful-unique-validation')

var commentSchema = new Schema({
  part_no: { type: String, ref: 'Partno', required: true },
  username: { type: String, ref: 'Username', required: true },
  content: { type: String, required: true },
  created: Date,
  updated: Date
})

// Beautify duplication errors
commentSchema.plugin(beautifyUnique)

commentSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    ret.id = ret._id
    delete ret._id
    delete ret.__v
    return ret
  }
})

commentSchema.pre('save', function (next) {
  var currentDate = new Date()
  this.updated = currentDate
  if (!this.created) {
    this.created = currentDate
  }
  next()
})
var Comment = mongoose.model('Comment', commentSchema)

module.exports = Comment

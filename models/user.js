var mongoose = require('mongoose')
var Schema = mongoose.Schema

var beautifyUnique = require('mongoose-beautiful-unique-validation')

var userSchema = new Schema({
  username: { type: String, required: true, unique: true },
  hash_password: { type: String, default: '', select: false },
  email: { type: String, required: true, unique: true },
  first_name: { type: String, default: '' },
  last_name: { type: String, default: '' },
  admin: { type: Boolean, default: false },
  created: Date,
  updated: Date
})

// Beautify duplication errors
userSchema.plugin(beautifyUnique)

userSchema.set('toJSON', {
  transform: function (doc, ret, options) {
    delete ret._id
    delete ret.__v
    delete ret.hash_password
    return ret
  }
})

userSchema.pre('save', function (next) {
  var currentDate = new Date()
  this.updated = currentDate
  if (!this.created) {
    this.created = currentDate
  }
  next()
})
var User = mongoose.model('User', userSchema)

module.exports = User

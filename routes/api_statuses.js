var express = require('express')
var router = express.Router()

var Status = require('../models/status')

var statusFields = 'code description'

router.param('status_code', function (req, res, next, scode) {
  Status.findOne({'code': scode}, statusFields, function (err, sta) {
    if (!err) { req.foundStatus = sta; next() } else { res.status(404).end() }
  })
  next()
})

router.route('/')
/* GET all statuses */
.get(function (req, res, next) {
  var querySet = {}
  if (req.query.code) { querySet.part_no = req.query.part_no }
  Status.find(querySet, function (err, statuses) {
    if (err) throw err
    res.status(200).json(statuses)
  }).select(statusFields)
})
/* POST create new status */
.post(function (req, res, next) {
  var query = {}
  if (req.body.description) { query.description = req.body.description }
  if (req.body.code) { query.code = req.body.code }
  var newStatus = new Status(query)
  newStatus.save(function (err) {
    var resStatus = 400; var resContent = {'messages': []}
    if (err) {
      for (var prop in err.errors) {
        if (err.errors[prop].message) {
          var errorContent = { 'name': prop, 'error': err.errors[prop].message }
          resContent.messages.push(errorContent)
        }
      }
    } else {
      resStatus = 201; resContent = newStatus
    }
    res.status(resStatus).json(resContent)
  })
})

router.route('/:status_code')
/* GET one status by ID */
.get(function (req, res, next) {
  res.status(200).json(req.foundStatus)
})

module.exports = router

var express = require('express')
var router = express.Router()

var Comment = require('../models/comment')

var commentFields = 'id username part content'

router.param('comment_id', function (req, res, next, id) {
  Comment.findOne({'_id': id}, commentFields, function (err, comment) {
    if (!err) { req.foundComment = comment; next() } else { res.status(404).end() }
  })
})

router.route('/')
/* GET all comments */
.get(function (req, res, next) {
  var querySet = {}
  if (req.query.username) { querySet.username = req.query.username }
  if (req.query.content) { querySet.content = {$regex: req.query.content} }
  if (req.query.part_no) { querySet.part_no = req.query.part_no }
  Comment.find(querySet, function (err, comments) {
    if (err) { throw err } else { res.status(200).json(comments) }
  }).select(commentFields)
})

router.route('/:comment_id')
/* GET one comment by ID */
.get(function (req, res, next) {
  res.status(200).json(req.foundComment)
})

module.exports = router

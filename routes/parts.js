var express = require('express')
var router = express.Router()

var Part = require('../models/part')

router.param('part_no', function (req, res, next, partNo) {
  Part.findOne({'part_no': partNo}, function (err, part) {
    if (err) { console.log(err) } else { req.foundPart = part }
    next()
  })
})

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.render('parts_list', {})
})

router.get('/:part_no', function (req, res, next) {
  res.render('parts_detail', {'part': req.foundPart})
})

module.exports = router

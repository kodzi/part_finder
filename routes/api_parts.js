var express = require('express')
var router = express.Router()

var Part = require('../models/part')
var User = require('../models/user')
var Status = require('../models/status')
var Comment = require('../models/comment')

var partFields = 'part_no rfid status description'

router.param('part_no', function (req, res, next, partNo) {
  Part.findOne({'part_no': partNo}, partFields, function (err, part) {
    if (!err) { req.foundPart = part; next() } else { res.status(404).end() }
  })
})

router.route('/')
/* GET parts listing, allows queries: rfid, status_id, description */
.get(function (req, res, next) {
  var querySet = {}
  if (req.query.rfid) { querySet.rfid = req.query.rfid }
  if (req.query.status_id) { querySet.status_id = req.query.status_id }
  if (req.query.description) {
    querySet.description = {$regex: req.query.description}
  }
  Part.find(querySet, function (err, parts) {
    if (err) { throw err } else { res.status(200).json(parts) }
  }).select(partFields)
})
/* POST create new part. */
.post(function (req, res, next) {
  var query = {}
  if (req.body.status) { query.status = req.body.status }
  if (req.body.rfid) { query.rfid = req.body.rfid }
  if (req.body.description) { query.description = req.body.description }
  if (req.body.part_no) { query.part_no = req.body.part_no }
  var newPart = new Part(query)
  newPart.save(function (err) {
    if (err) {
      var resStatus = 400; var resContent = {'messages': []}
      if (err.name === 'ValidationError') { resStatus = 409 }
      for (var prop in err.errors) {
        if (err.errors[prop].message) {
          var errorContent = { 'name': prop, 'error': err.errors[prop].message }
          resContent.messages.push(errorContent)
        }
      }
    } else {
      resStatus = 201; resContent = newPart
    }
    res.status(resStatus).json(resContent)
  })
})

router.route('/:part_no')
/* GET one part by ID */
.get(function (req, res, next) {
  res.status(200).json(req.foundPart)
})
/*  PUT (NOT WORKING) modify part's info: allows changing status and description */
.put(function (req, res, next) {
  if (req.body.status) {
    var resStatus = 200; var resContent = {'message': []}
    getStatus(req.body.status, function (err, fstatus) {
      if (err) {
        resStatus = 500; resContent.message = [{'name': 'general', 'error': err.errors}]
      } else if (!fstatus) {
        resStatus = 404; resContent.message = [{'name': 'status', 'error': 'Statuscode does not exist, aborting.'}]
      } else {
        if (req.body.description) { req.foundPart.description = req.body.description }
        req.foundPart.status = fstatus.code; req.foundPart.save()
        resContent = req.foundPart
      }
      res.status(resStatus).json(resContent)
    })
  } else {
    if (req.body.description) {
      req.foundPart.description = req.body.description; req.foundPart.save()
    }
    res.status(200).json(req.foundPart)
  }
})

router.route('/:part_no/comments')
/* GET all comments for part */
.get(function (req, res, next) {
  getComments(req.foundPart.part_no, function (err, comments) {
    var resStatus = 500; var resContent = {'messages': []}
    if (err) {
      resStatus = 400; resContent.messages.push({'name': 'general', 'error': err.message})
    } else { resStatus = 200; resContent = comments }
    res.status(resStatus).json(resContent)
  })
})
/* POST create new comment for part */
.post(function (req, res, next) {
  getUser(req.body.username, function (err, user) {
    if (err) {
      res.status(400).json({'messages': [{'name': 'general', 'error': err.message}]})
    } else if (!user) {
      res.status(404).json({'messages': [{'name': 'username', 'error': 'User not found'}]})
    } else {
      var query = {'username': user.username, 'part_no': req.foundPart.part_no}
      if (req.body.content) { query.content = req.body.content }
      var newComment = new Comment(query)
      newComment.save(function (erro) {
        var resStatus = 201; var resContent = {'messages': []}
        if (erro) {
          for (var prop in erro.errors) {
            if (erro.errors[prop].message) {
              var errorContent = { 'name': prop, 'error': erro.errors[prop].message }
              resContent.messages.push(errorContent)
            }
          }
          resStatus = 400
        } else { resContent = newComment }
        res.status(resStatus).json(resContent)
      })
    }
  })
})

var getUser = function (username, next) {
  User.findOne({ 'username': username }, function (err, user) {
    if (err) { console.log(err) }
    next(err, user)
  })
}

var getComments = function (partNo, next) {
  Comment.find({ 'part_no': partNo }, '-part_no', function (err, coms) {
    if (err) { console.log(err) }
    next(err, coms)
  })
}

var getStatus = function (statusCode, next) {
  Status.findOne({'code': statusCode}, function (err, status) {
    if (err) { console.log(err) }
    next(err, status)
  })
}
module.exports = router

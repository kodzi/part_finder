var express = require('express')
var router = express.Router()

var User = require('../models/user')

var userFields = 'username first_name last_name email'

router.param('username', function (req, res, next, username) {
  User.findOne({'username': username}, userFields, function (err, user) {
    if (!err) { req.foundUser = user; next() } else { res.status(404).end() }
  })
})

router.route('/')
.get(function (req, res, next) {
  var querySet = {}
  if (req.query.username) { querySet.username = {'$regex': req.query.username} }
  if (req.query.first_name) { querySet.first_name = {'$regex': req.query.first_name} }
  if (req.query.last_name) { querySet.last_name = {'$regex': req.query.last_name} }
  User.find(querySet, function (err, users) {
    if (err) { throw err } else { res.status(200).json(users) }
  }).select(userFields)
})
/* POST create new user */
.post(function (req, res, next) {
  var query = {}
  if (req.body.username) { query.username = req.body.username }
  if (req.body.first_name) { query.first_name = req.body.first_name }
  if (req.body.last_name) { query.last_name = req.body.last_name }
  if (req.body.email) { query.email = req.body.email }
  if (req.body.admin && req.body.admin.toLowerCase() === 'true') { query.admin = true }
  var newUser = new User(query)
  newUser.save(function (err) {
    if (err) {
      var resStatus = 400; var resContent = {'messages': []}
      for (var prop in err.errors) {
        if (err.errors[prop].message) {
          var errorContent = { 'name': prop, 'error': err.errors[prop].message }
          resContent.messages.push(errorContent)
        }
      }
    } else {
      resStatus = 201
      resContent = newUser
    }
    res.status(resStatus).json(resContent)
  })
})

router.route('/:user_id')
/* GET user by ID */
.get(function (req, res, next) {
  res.status(200).json(req.foundUser)
})

module.exports = router

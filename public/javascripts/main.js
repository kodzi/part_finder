var highlightFormInputs = function (id, feedbackTag, message) {
  if (!$('#' + id)) { return }
  var holder = $('#' + id).closest('.form-group')
  if (holder) { holder.addClass('has-' + feedbackTag) }
  if (message) {
    if (feedbackTag === 'error') { feedbackTag = 'danger' }
    $('#' + id + '_helpblock')
    .removeClass('hidden').addClass('text-' + feedbackTag).html(message) }
}

$(document).ready(function () {
  var currentPath = window.location.pathname.split('/')[1]
  console.log(currentPath)
  $.each($('#navbar ul li a'), function (key, value) {
    var lipath = value.href.split(value.host)[1].split('/')[1]
    console.log(lipath)
    if (lipath.length === 0 && currentPath.length === 0) {
      console.log('first link')
      $(this).parent().addClass('active')
    } else if (currentPath.length > 0 && lipath.endsWith(currentPath)) {
      console.log('other links')
      $(this).parent().addClass('active')
    }
  })
})

/*global moment*/
/*global highlightFormInputs*/

var createNewComment = function () {
  $.ajax({
    type: 'POST',
    url: '/api/parts/' + $('#comments_new_form').data('part-no') + '/comments',
    data: $('#comments_new_form').serialize(),
    contentType: 'application/x-www-form-urlencoded',
    error: function (jqXHR, textStatus, errorThrown) {
      var json = $.parseJSON(jqXHR.responseText)
      $.each(json.messages, function (key, value) {
        var errorText = value.error
        if (value.error.indexOf('is required')) { errorText = 'This field is required' }
        highlightFormInputs(value.name, 'error', errorText)
      })
    },
    success: function (data, textStatus, jqXHR) {
      $('.help-block').addClass('hidden')
      $('.has-error').removeClass('has-error')
      $('#content').val('')
      populateComments()
    }
  })
}

var populateComments = function () {
  $.ajax({
    type: 'GET',
    url: '/api/parts/' + $('#comments_new_form').data('part-no') + '/comments',
    error: function (jqXHR, textStatus, errorThrown) {
      // console.log(textStatus)
      // console.log(errorThrown)
      // console.log(jqXHR)
    },
    success: function (data, textStatus, jqXHR) {
      // console.log('hello')
      $('#comments_container').html('')
      $.each(data, function (key, value) {
        insertComment(value)
      })
    }
  })
}

var insertComment = function (comment) {
  // console.log(1)
  var ctn = $('#comments_container')
  var cmctn = $('<div></div>').addClass('comment-container')
  var cmusr = $('<div></div>').addClass('comment-user').append($('<i class="icon"></i>').addClass('fa fa-user fa-3x'))
  var cmcon = $('<div></div>').addClass('comment-content clearfix')
  // console.log(2)
  var usrname = $('<a></a>').prop('href', '#').append(comment.username)
  cmcon.append($('<div></div>').addClass('comment-author').append(usrname))
  var comtxt = $('<div></div>').addClass('comment-text').append(comment.content)
  cmcon.append(comtxt)
  var datetime = moment(comment.created).fromNow()
  var cmdate = $('<div></div>').addClass('comment-created').addClass('datetime-objects').append(datetime)
  cmcon.append(cmdate)
  cmctn.append(cmusr)
  cmctn.append(cmcon)
  ctn.append(cmctn)
  $('#comments_container').scrollTop($('#comments_container')[0].scrollHeight)
}

var changeStatus = function (statusCode) {
  $.ajax({
    type: 'PUT',
    url: '/api/parts/' + $('#comments_new_form').data('part-no'),
    data: {'status': statusCode},
    contentType: 'application/x-www-form-urlencoded',
    error: function (jqXHR, textStatus, errorThrown) {
      var json = $.parseJSON(jqXHR.responseText)
      $.each(json.messages, function (key, value) {
        var errorText = value.error
        if (value.error.indexOf('is required')) { errorText = 'This field is required' }
        highlightFormInputs('status', 'error', errorText)
      })
    },
    success: function (data, textStatus, jqXHR) {
      $('#status_helpblock').addClass('hidden')
      $('#status').data('initial-value', statusCode)
    }
  })
}

$(document).ready(function () {
  populateComments()
  $('#comments_new_submit').on('click', function (evt) {
    evt.preventDefault()
    createNewComment()
  })

  $('#username').select2({
    placeholder: 'Select an username',
    theme: 'bootstrap',
    width: '100%',
    ajax: {
      url: '/api/users',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return { username: params.term }
      },
      processResults: function (data, params) {
        // results expected to be [{id, text}]
        var newData = []
        $.each(data, function (key, value) {
          newData.push({ id: value.username, text: value.username })
        })
        return { results: newData }
      },
      cache: false
    }
  })

  $('#status').select2({
    theme: 'bootstrap',
    width: '100%',
    ajax: {
      url: '/api/statuses',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return { username: params.term }
      },
      processResults: function (data, params) {
        // results expected to be [{id, text}]
        var newData = []
        $.each(data, function (key, value) {
          newData.push({ id: value.code, text: value.code + ' - ' + value.description })
        })
        return { results: newData }
      },
      cache: false
    }
  })
  console.log($('#status').data('initial-value'))
  $('#status').val($('#status').data('initial-value')).trigger('change.select2')
  $('#status').on('change', function (e) {
    if ($(this).val() === $(this).data('initial-value')) {
      return
    } else {
      changeStatus($(this).val())
    }
  })
})

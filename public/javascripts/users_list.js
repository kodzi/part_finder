/*global highlightFormInputs*/

var createNewUser = function () {
  $.ajax({
    type: 'POST',
    url: '/api/users/',
    data: $('#users_new_form').serialize(),
    contentType: 'application/x-www-form-urlencoded',
    error: function (jqXHR, textStatus, errorThrown) {
      var json = $.parseJSON(jqXHR.responseText)
      $.each(json.messages, function (key, value) {
        var errorText = value.error
        if (value.error.indexOf('is required')) { errorText = 'This field is required' }
        highlightFormInputs(value.name, 'error', errorText)
      })
    },
    success: function (data, textStatus, jqXHR) {
      $('.help-block').removeClass().addClass('help-block hidden')
      $('.form-group').removeClass().addClass('form-group')
      populateUsers()
    }
  })
}

var populateUsers = function () {
  $.ajax({
    type: 'GET',
    url: '/api/users/',
    error: function (jqXHR, textStatus, errorThrown) {
      // console.log(textStatus)
      // console.log(errorThrown)
      // console.log(jqXHR)
    },
    success: function (data, textStatus, jqXHR) {
      // console.log('hello')
      $('#users_listing_table tbody').html('')
      $.each(data, function (key, value) {
        insertUser(value)
      })
    }
  })
}

var insertUser = function (user) {
  // tr
  //   td #{user.username}
  //   td #{user.first_name}
  //   td #{user.last_name}
  //   td
  //     a(href="mailto:#{user.email}") #{user.email}
  var ctn = $('#users_listing_table tbody')
  var tr = $('<tr></tr>')
  tr.append($('<td></td>').append(user.username))
  tr.append($('<td></td>').append(user.first_name))
  tr.append($('<td></td>').append(user.last_name))
  tr.append($('<td></td>').append($('<a></a>').prop('href', 'mailto:' + user.email).append(user.email)))
  ctn.append(tr)
}

$(document).ready(function () {
  populateUsers()
  // Bind buttons
  $('#users_new_submit').on('click', function () {
    createNewUser()
  })
})

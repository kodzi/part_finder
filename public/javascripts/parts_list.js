/*global highlightFormInputs*/

var createNewPart = function () {
  $.ajax({
    type: 'POST',
    url: '/api/parts/',
    data: $('#parts_new_form').serialize(),
    contentType: 'application/x-www-form-urlencoded',
    error: function (jqXHR, textStatus, errorThrown) {
      var json = $.parseJSON(jqXHR.responseText)
      $.each(json.messages, function (key, value) {
        var errorText = value.error
        if (value.error.indexOf('is required')) { errorText = 'This field is required' }
        highlightFormInputs(value.name, 'error', errorText)
      })
    },
    success: function (data, textStatus, jqXHR) {
      $('.help-block').removeClass().addClass('help-block hidden')
      $('.form-group').removeClass().addClass('form-group')
      populateParts()
    }
  })
}

var populateParts = function () {
  $.ajax({
    type: 'GET',
    url: '/api/parts/',
    error: function (jqXHR, textStatus, errorThrown) {
      // console.log(textStatus)
      // console.log(errorThrown)
      // console.log(jqXHR)
    },
    success: function (data, textStatus, jqXHR) {
      // console.log('hello')
      $('#parts_listing_table tbody').html('')
      $.each(data, function (key, value) {
        insertPart(value)
      })
    }
  })
}

var insertPart = function (part) {
  // tr(title='RFID=#{part.rfid}')
  //   td #{part.part_no}
  //   td #{part.description}
  //   td #{part.status}
  //   td
  //     a.center-aligned(href='/parts/#{part.part_no}') View
  var ctn = $('#parts_listing_table tbody')
  var tr = $('<tr></tr>')
  tr.append($('<td></td>').append(part.part_no))
  tr.append($('<td></td>').append(part.description))
  tr.append($('<td></td>').append(part.status))
  tr.append($('<td></td>').append($('<a></a>').prop('href', '/parts/' + part.part_no).prop('title', 'View comments').append('View')))
  ctn.append(tr)
}

$(document).ready(function () {
  populateParts()
  // Bind buttons
  $('#parts_new_submit').on('click', function () {
    createNewPart()
  })
  $('#status').select2({
    placeholder: 'Select a status',
    theme: 'bootstrap',
    width: '100%',
    ajax: {
      url: '/api/statuses',
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return { username: params.term }
      },
      processResults: function (data, params) {
        // results expected to be [{id, text}]
        var newData = []
        $.each(data, function (key, value) {
          newData.push({ id: value.code, text: value.code + ' - ' + value.description })
        })
        return { results: newData }
      },
      cache: false
    }
  })
})

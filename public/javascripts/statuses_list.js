/*global highlightFormInputs*/

var createNewStatus = function () {
  $.ajax({
    type: 'POST',
    url: '/api/statuses/',
    data: $('#statuses_new_form').serialize(),
    contentType: 'application/x-www-form-urlencoded',
    error: function (jqXHR, textStatus, errorThrown) {
      var json = $.parseJSON(jqXHR.responseText)
      $.each(json.messages, function (key, value) {
        var errorText = value.error
        if (value.error.indexOf('is required')) { errorText = 'This field is required' }
        highlightFormInputs(value.name, 'error', errorText)
      })
    },
    success: function (data, textStatus, jqXHR) {
      $('.help-block').removeClass().addClass('help-block hidden')
      $('.form-group').removeClass().addClass('form-group')
      populateStatuses()
    }
  })
}

var populateStatuses = function () {
  $.ajax({
    type: 'GET',
    url: '/api/statuses/',
    error: function (jqXHR, textStatus, errorThrown) {
      // console.log(textStatus)
      // console.log(errorThrown)
      // console.log(jqXHR)
    },
    success: function (data, textStatus, jqXHR) {
      // console.log('hello')
      $('#statuses_listing_table tbody').html('')
      $.each(data, function (key, value) {
        insertStatus(value)
      })
    }
  })
}

var insertStatus = function (status) {
  var ctn = $('#statuses_listing_table tbody')
  var tr = $('<tr></tr>')
  tr.append($('<td></td>').append(status.code))
  tr.append($('<td></td>').append(status.description))
  ctn.append(tr)
}

$(document).ready(function () {
  populateStatuses()
  // Bind buttons
  $('#statuses_new_submit').on('click', function () {
    createNewStatus()
  })
})

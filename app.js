var express = require('express')
var path = require('path')
// var favicon = require('serve-favicon')
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var config
try {
  config = require('./configs_local')
} catch (e) {
  console.log('WARNING: No local configurations "configs_local" available, using default!')
  config = require('./configs')
}

var app = express()

// db engine setup
var mongoose = require('mongoose')
mongoose.connect('mongodb://' + config.mongodb.server + '/' + config.mongodb.project)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

// uncomment after placing your favicon in /public
// app.use(favicon(__dirname + '/public/favicon.ico'))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', require('./routes/index'))
app.use('/parts', require('./routes/parts'))
app.use('/users', require('./routes/users'))
app.use('/statuses', require('./routes/statuses'))

app.use('/api/comments', require('./routes/api_comments'))
app.use('/api/parts', require('./routes/api_parts'))
app.use('/api/statuses', require('./routes/api_statuses'))
app.use('/api/users', require('./routes/api_users'))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
